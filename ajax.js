//usando ajax
var getdetalles = function(metros){
    $.ajax({
        url: 'casas.php?metros='+metros,
        method: 'get',
        dataType:'json',
        success: function(data) {
            console.log(data);
            resultado(data);
        },
        error:function(){
            console.log("Error");
        }
    });
}

var resultado = function(data)  {
    $("div.info").html('').show();
    $.each(data,function(index,value) {
        $("div.info").append("<div class=\"resultado\">");
        $("div.info").append("\tId: ",data[index].id);
        $("div.info").append("\tnombre: ",data[index].nombre);
        $("div.info").append("\tinfo: ",data[index].info);
        $("div.info").append("\thoras: ",data[index].horas);
        $("div.info").append("</div>");
        console.log("index es",index, "value es",value);
    });
}
/*
usando getJSON
var getdetails = function(metros){
    $.getJSON(
        "casas.php",
        "metros="+metros,
        function(data){
            restults(data);
        }
    )};

var restults = function(data)  {
    $("div.info").html('').show();
    $.each(data,function(index,value) {
        $("div.info").append("<div class=\"resultado\">");
        $("div.info").append("ID: "+data[index].id);
        $("div.info").append("M2: "+data[index].nombre);
        $("div.info").append("Habitaciones: "+data[index].info);
        $("div.info").append("Localización: "+data[index].horas);
        $("div.info").append("</div>");
    });
}
*/

