$(document).ready(function(){
	$('#actualizar').click(function(e) {			
			console.log("Cargando lista de datos....");
			$.ajax({
		        url: 'articulos.php?obtenerLista=1',
		        method: 'get',
		        dataType:'json',
		        success: function(data) {
		            //console.log(data);
		            cargar_listado(data);		            
					/*$("#listadoArticulos").html('');
		            $.each(data,function(index,value) {
	        			$("#listadoArticulos").append('<li><a href="#">'+data[index].id+'. '+data[index].nombre+'<span class="ui-li-count">'+data[index].valor+'</span></a></li>');
			    	});
					$("#listadoArticulos").listview();
					$("#listadoArticulos").listview("refresh");*/
		        },
		        error:function(){
		            console.log("Error");
		        }
	    	});
	});

	function cargar_listado (data) {
		console.log("Cargando lista de datos....");
		$.mobile.loadingMessageTextVisible = true;
		$.mobile.showPageLoadingMsg("a", "Actualizando datos espere...", false);
		$("#listadoArticulos").html('');
		$.each(data,function(index,value) {
	        $("#listadoArticulos").append('<li><a href="#">'+data[index].id+'. '+data[index].nombre+'<span class="ui-li-count">'+data[index].valor+'</span></a></li>');
		});
		$("#listadoArticulos").listview();
		$("#listadoArticulos").listview("refresh");
		$.mobile.hidePageLoadingMsg();						 
	}
});
