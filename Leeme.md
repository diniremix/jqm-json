##Ejemplo de JSON y JQuery Mobile

El ejemplo consta de varios archivos

- index.html
- getjson.js
- articulos.php
- resultado.json

y un script con la base de datos llamado **database.sql**

En el **index.html** tiene una sola pagina y dentro de ella una lista **ul** llamada **listadoArticulos**.

El archivo **getjson.js** contiene la llamada a **AJAX** necesaria para traerse un **JSON** desde el servidor, la funcion **success** de AJAX que se ejecuta en caso de que la llamada haya sido satisfactoria llama a la funcion **cargar_listado** y le envia el **JSON** que devolvió el servidor y esta se encarga de organizar los datos y mostrarlos sobre la lista **ul**

El archivo **articulos.php** contiene la implementacion de **PHP** necesaria para conectarse a la base de datos, hacer uso de la funcion **mysql_fetch_assoc()** para organizar el resultado de la consulta y la funcion **json_encode()** que se encarga de convertir a formato **JSON** un array de datos y finalmente devolverlo al cliente.

El archivo **resultado.json** contiene los resultados devueltos por el servidor en formato **JSON**


Mas info:

- manejo de listas en **JQuery Mobile**
   - http://jquerymobile.com/demos/1.2.0/docs/lists/docs-lists.html

- **AJAX con JQuery**
   - http://api.jquery.com/jQuery.ajax/
   - http://www.w3schools.com/jquery/jquery_ajax_intro.asp

- **PHP y JSON**
   - http://php.net/manual/es/ref.json.php

Cualquier duda podes encontrarme en [Twitter](https://twitter.com/diniremix) y [Google+](http://gplus.to/diniremix)
